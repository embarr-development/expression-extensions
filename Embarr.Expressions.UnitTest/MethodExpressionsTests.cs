﻿using System;
using System.Linq.Expressions;
using Embarr.Expressions.UnitTest.Models;
using NUnit.Framework;
using Should;

namespace Embarr.Expressions.UnitTest
{
    [TestFixture]
    public class MethodExpressionsTests
    {
        public class GetMethodInfo : MethodExpressionsTests
        {
            [Test]
            public void Should_Get_Top_Level_Method_Info()
            {
                // Arrange
                Expression<Action<TestModel>> myMethodExpression = x => x.MyMethod("123");

                // Act
                var methodInfo = myMethodExpression.GetMethodInfo();

                // Assert
                methodInfo.Name.ShouldEqual("MyMethod");
            }

            [Test]
            public void Should_Get_Sub_Level_Method_Info()
            {
                // Arrange
                Expression<Action<TestModel>> myMethodExpression = x => x.SubTestModel.MySubMethod("123");

                // Act
                var methodInfo = myMethodExpression.GetMethodInfo();

                // Assert
                methodInfo.Name.ShouldEqual("MySubMethod");
            }
        }

        public class GetMethodInfoWithGenericArgument : MethodExpressionsTests
        {
            [Test]
            public void Should_Get_Top_Level_Method_Info()
            {
                // Act
                var methodInfo = MethodExpressions.GetMethodInfo<TestModel>(x => x.MyMethod(null));

                // Assert
                methodInfo.Name.ShouldEqual("MyMethod");
            }

            [Test]
            public void Should_Get_Sub_Level_Method_Info()
            {
                // Act
                var methodInfo = MethodExpressions.GetMethodInfo<TestModel>(x => x.SubTestModel.MySubMethod("123"));

                // Assert
                methodInfo.Name.ShouldEqual("MySubMethod");
            }
        }

        public class IsMethod : MethodExpressionsTests
        {
            [Test]
            public void Should_Return_True_If_Expression_Is_A_Method()
            {
                // Arrange
                Expression<Action<TestModel>> myMethodExpression = x => x.MyMethod("123");

                // Act
                var isMethod = MethodExpressions.IsMethod(myMethodExpression);

                // Assert
                isMethod.ShouldBeTrue();
            }
        }

        public class IsMethodWithGenericArgument : MethodExpressionsTests
        {
            [Test]
            public void Should_Return_True_If_Expression_Is_A_Method()
            {
                // Act
                var isMethod = MethodExpressions.IsMethod<TestModel>(x => x.MyStringMethod("Test"));

                // Assert
                isMethod.ShouldBeTrue();
            }
        }

        public class ToMethodCallExpression : MethodExpressionsTests
        {
            [Test]
            public void Should_Return_MethodExpression_From_Valid_Method_Expression()
            {
                // Arrange
                Expression<Action<TestModel>> myMethodExpression = x => x.MyMethod("123");

                // Act
                var methodCallExpression = myMethodExpression.ToMethodCallExpression();

                // Assert
                methodCallExpression.ShouldNotBeNull();
            }
        }

        public class ToMethodCallExpressionWithGenericArgument : MethodExpressionsTests
        {
            [Test]
            public void Should_Return_MethodExpression_From_Valid_Method_Expression()
            {
                // Act
                var methodCallExpression = MethodExpressions.ToMethodCallExpression<TestModel>(x => x.MyMethod("test"));

                // Assert
                methodCallExpression.ShouldNotBeNull();
            }
        }

        public class CallMethod : MethodExpressionsTests
        {
            [Test]
            public void Should_Call_Void_Method()
            {
                // Arrange
                var testModel = new TestModel();
                Expression<Action<TestModel>> myMethodExpression = x => x.MyMethod("123");

                // Act
                myMethodExpression.CallMethod(testModel);

                // Assert
                testModel.MyMethodValue.ShouldEqual("123");
            }

            [Test]
            public void Should_Call_Void_Method_With_Generic_Argument()
            {
                // Arrange
                var testModel = new TestModel();
                Expression<Action<TestModel>> myMethodExpression = x => x.MyMethod("123");

                // Act
                MethodExpressions.CallMethod(myMethodExpression, testModel);

                // Assert
                testModel.MyMethodValue.ShouldEqual("123");
            }

            [Test]
            public void Should_Call_Method_With_Return()
            {
                // Arrange
                var testModel = new TestModel();
                Expression<Func<TestModel, string>> myMethodExpression = x => x.MyStringMethod("123");

                // Act
                var result = myMethodExpression.CallMethod<TestModel, string>(testModel);

                // Assert
                result.ShouldEqual("123");
            }

            [Test]
            public void Should_Call_Method_With_Generic_Argument_With_Return()
            {
                // Arrange
                const int indexItem = 0;
                var testModel = new TestModel {SubTestModel = new SubTestModel()};
                Expression<Func<TestModel, string>> myMethodExpression = x => x.SubTestModelList[indexItem].MySubMethod(GetValue());

                // Act
                var result = MethodExpressions.CallMethod(myMethodExpression, testModel);

                // Assert
                result.ShouldEqual("123");
            }

            private static string GetValue()
            {
                return "123";
            }
        }
    }
}