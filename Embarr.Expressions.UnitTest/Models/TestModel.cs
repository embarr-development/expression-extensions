﻿using System.Collections.Generic;

namespace Embarr.Expressions.UnitTest.Models
{
    public class TestModel
    {
        public string TopLevelStringProperty { get; set; } 
        public int TopLevelIntProperty { get; set; } 
        public int? TopLevelNullableIntProperty { get; set; }
        public SubTestModel SubTestModel { get; set; }
        public List<SubTestModel> SubTestModelList { get; set; }
        public string MyMethodValue { get; set; }

        public TestModel()
        {
            SubTestModelList = new List<SubTestModel>();
            SubTestModelList.Add(new SubTestModel());
        }

        public void MyMethod(string value)
        {
            MyMethodValue = value;
        }

        public string MyStringMethod(string value)
        {
            MyMethodValue = value;
            return value;
        }
    }

    public class SubTestModel
    {
        public string SubLevelStringProperty { get; set; }
        public int SubLevelIntProperty { get; set; }
        public int? SubLevelNullableIntProperty { get; set; }
        public string MyMethodValue { get; set; }
        public string MySubMethod(string value)
        {
            MyMethodValue = value;
            return value;
        }
    }
}