﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Embarr.Expressions.UnitTest.Models;
using NUnit.Framework;
using Should;

namespace Embarr.Expressions.UnitTest
{
    [TestFixture]
    public class PropertyExpressionsTests
    {
        public class GetPropertyInfo : PropertyExpressionsTests
        {
            [Test]
            public void Should_Get_Top_Level_String_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, string>> myPropertyExpression = x => x.TopLevelStringProperty;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();
                
                // Assert
                result.Name.ShouldEqual("TopLevelStringProperty");
            }

            [Test]
            public void Should_Get_Top_Level_Int_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, int>> myPropertyExpression = x => x.TopLevelIntProperty;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();

                // Assert
                result.Name.ShouldEqual("TopLevelIntProperty");
            }

            [Test]
            public void Should_Get_Top_Level_Nullable_Int_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, int?>> myPropertyExpression = x => x.TopLevelNullableIntProperty;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();

                // Assert
                result.Name.ShouldEqual("TopLevelNullableIntProperty");
            }

            [Test]
            public void Should_Get_Top_Level_Complex_Model_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModel;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();

                // Assert
                result.Name.ShouldEqual("SubTestModel");
            }

            [Test]
            public void Should_Get_Top_Level_Complex_Model_List_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModelList;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();

                // Assert
                result.Name.ShouldEqual("SubTestModelList");
            }

            [Test]
            public void Should_Get_Sub_Level_String_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModel.SubLevelStringProperty;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();

                // Assert
                result.Name.ShouldEqual("SubLevelStringProperty");
            }

            [Test]
            public void Should_Get_Sub_Level_String_From_List_PropertyInfo()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModelList[0].SubLevelStringProperty;

                // Act
                var result = myPropertyExpression.GetPropertyInfo();

                // Assert
                result.Name.ShouldEqual("SubLevelStringProperty");
            }
        }

        public class GetPropertyInfoInlineExpression : PropertyExpressionsTests
        {
            [Test]
            public void Should_Get_Top_Level_String_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.TopLevelStringProperty);

                // Assert
                result.Name.ShouldEqual("TopLevelStringProperty");
            }

            [Test]
            public void Should_Get_Top_Level_Int_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.TopLevelIntProperty);

                // Assert
                result.Name.ShouldEqual("TopLevelIntProperty");
            }

            [Test]
            public void Should_Get_Top_Level_Nullable_Int_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.TopLevelNullableIntProperty);

                // Assert
                result.Name.ShouldEqual("TopLevelNullableIntProperty");
            }

            [Test]
            public void Should_Get_Top_Level_Complex_Model_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.SubTestModel);

                // Assert
                result.Name.ShouldEqual("SubTestModel");
            }

            [Test]
            public void Should_Get_Top_Level_Complex_Model_List_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.SubTestModelList);

                // Assert
                result.Name.ShouldEqual("SubTestModelList");
            }

            [Test]
            public void Should_Get_Sub_Level_String_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.SubTestModel.SubLevelStringProperty);

                // Assert
                result.Name.ShouldEqual("SubLevelStringProperty");
            }

            [Test]
            public void Should_Get_Sub_Level_String_From_List_PropertyInfo()
            {
                // Act
                var result = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.SubTestModelList[0].SubLevelStringProperty);

                // Assert
                result.Name.ShouldEqual("SubLevelStringProperty");
            }
        }

        public class IsProperty : PropertyExpressionsTests
        {
            [Test]
            public void Should_Be_True_When_Using_IsProperty_Method()
            {
                // Act
                var result = PropertyExpressions.IsProperty<TestModel, string>(x => x.SubTestModelList[0].SubLevelStringProperty);

                // Assert
                result.ShouldBeTrue();
            }

            [Test]
            public void Should_Be_True_When_Using_IsProperty_Extensions_Method()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModelList;

                // Act
                var result = myPropertyExpression.IsProperty();

                // Assert
                result.ShouldBeTrue();
            }
        }

        public class IsPropertyIndexer : PropertyExpressionsTests
        {
            [Test]
            public void Should_Be_True_When_Property_Is_Indexer()
            {
                // Act
                var result = PropertyExpressions.IsPropertyIndexer<TestModel, object>(x => x.SubTestModelList[0]);

                // Assert
                result.ShouldBeTrue();
            }

            [Test]
            public void Should_Be_True_When_Property_Is_Indexer_Using_Extension_Method()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModelList[0];

                // Act
                var result = myPropertyExpression.IsPropertyIndexer();

                // Assert
                result.ShouldBeTrue();
            }
            
            [Test]
            public void Should_Be_False_When_Property_Is_Not_Indexer()
            {
                // Act
                var result = PropertyExpressions.IsPropertyIndexer<TestModel, string>(x => x.SubTestModelList[0].SubLevelStringProperty);

                // Assert
                result.ShouldBeFalse();
            }

            [Test]
            public void Should_Be_False_When_Property_Is_Not_Indexer_Using_Extension_Method()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModelList;

                // Act
                var result = myPropertyExpression.IsPropertyIndexer();

                // Assert
                result.ShouldBeFalse();
            }
        }

        public class GetValue : PropertyExpressionsTests
        {
            [Test]
            public void Should_Get_Value_From_Model()
            {
                // Arrange
                var testModel = new TestModel
                {
                    SubTestModel = new SubTestModel
                    {
                        SubLevelStringProperty = "MyString"
                    }
                };

                Expression<Func<TestModel, string>> myPropertyExpression = x => x.SubTestModel.SubLevelStringProperty;

                // Act
                var result = PropertyExpressions.GetValue(myPropertyExpression, testModel);

                // Assert
                result.ShouldEqual(testModel.SubTestModel.SubLevelStringProperty);
            }

            [Test]
            public void Should_Get_Value_From_Model_With_Extension_Method()
            {
                // Arrange
                var testModel = new TestModel
                {
                    SubTestModel = new SubTestModel
                    {
                        SubLevelStringProperty = "MyString"
                    }
                };

                Expression<Func<TestModel, string>> myPropertyExpression = x => x.SubTestModel.SubLevelStringProperty;

                // Act
                var result = myPropertyExpression.GetValue<TestModel, string>(testModel);

                // Assert
                result.ShouldEqual(testModel.SubTestModel.SubLevelStringProperty);
            }
        }

        public class ToMemberExpression : PropertyExpressionsTests
        {
            [Test]
            public void Should_Return_Member_Expression_With_Generic_Arguments()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.SubTestModelList;

                // Act
                var result = myPropertyExpression.ToMemberExpression();

                // Assert
                result.Member.Name.ShouldEqual("SubTestModelList");
            }

            [Test]
            public void Should_Return_Member_Expression()
            {
                // Act
                var result = PropertyExpressions.ToMemberExpression<TestModel, string>(x => x.SubTestModelList[0].SubLevelStringProperty);

                // Assert
                result.Member.Name.ShouldEqual("SubLevelStringProperty");
            }

            [Test]
            public void Should_Throw_Exception_If_Not_Property()
            {
                // Arrange
                Expression<Func<TestModel, object>> myPropertyExpression = x => x.MyStringMethod("123");

                // Act
                var result = Assert.Throws<ArgumentException>(() => myPropertyExpression.ToMemberExpression());

                // Assert
                result.Message.ShouldEqual("Only properties are supported");
            }
        }
    }
}
