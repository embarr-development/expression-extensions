﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Reflection;

namespace Embarr.Expressions.Cache
{
    internal static class Memoizer
    {
        static readonly ConcurrentDictionary<LambdaExpression, PropertyInfo> ExpressionToPropertyInfoMaps = new ConcurrentDictionary<LambdaExpression, PropertyInfo>();

        internal static Func<LambdaExpression, PropertyInfo> Memoize(this Func<LambdaExpression, PropertyInfo> function)
        {
            return param =>
            {
                PropertyInfo value;
                
                if (ExpressionToPropertyInfoMaps.TryGetValue(param, out value))
                {
                    return value;
                }
                
                value = function(param);
                ExpressionToPropertyInfoMaps.TryAdd(param, value);
                return value;
            };
        }

        static readonly ConcurrentDictionary<LambdaExpression, MemberExpression> ExpressionToMemberExpressionMaps = new ConcurrentDictionary<LambdaExpression, MemberExpression>();

        internal static Func<LambdaExpression, MemberExpression> Memoize(this Func<LambdaExpression, MemberExpression> function)
        {
            return param =>
            {
                MemberExpression value;

                if (ExpressionToMemberExpressionMaps.TryGetValue(param, out value))
                {
                    return value;
                }

                value = function(param);
                ExpressionToMemberExpressionMaps.TryAdd(param, value);
                return value;
            };
        }

        static readonly ConcurrentDictionary<Expression, MethodInfo> ExpressionToMethodInfoMaps = new ConcurrentDictionary<Expression, MethodInfo>();

        internal static Func<Expression, MethodInfo> Memoize(this Func<Expression, MethodInfo> function)
        {
            return param =>
            {
                MethodInfo value;

                if (ExpressionToMethodInfoMaps.TryGetValue(param, out value))
                {
                    return value;
                }

                value = function(param);
                ExpressionToMethodInfoMaps.TryAdd(param, value);
                return value;
            };
        }

        static readonly ConcurrentDictionary<LambdaExpression, bool> ExpressionToIsPropertyMaps = new ConcurrentDictionary<LambdaExpression, bool>();

        internal static Func<LambdaExpression, bool> Memoize(this Func<LambdaExpression, bool> function)
        {
            return param =>
            {
                bool value;

                if (ExpressionToIsPropertyMaps.TryGetValue(param, out value))
                {
                    return value;
                }

                value = function(param);
                ExpressionToIsPropertyMaps.TryAdd(param, value);
                return value;
            };
        }

        static readonly ConcurrentDictionary<Expression, bool> ExpressionIsMethodInfoMaps = new ConcurrentDictionary<Expression, bool>();

        internal static Func<Expression, bool> Memoize(this Func<Expression, bool> function)
        {
            return param =>
            {
                bool value;

                if (ExpressionIsMethodInfoMaps.TryGetValue(param, out value))
                {
                    return value;
                }

                value = function(param);
                ExpressionIsMethodInfoMaps.TryAdd(param, value);
                return value;
            };
        }
    }
}