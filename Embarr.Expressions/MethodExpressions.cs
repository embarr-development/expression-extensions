﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Embarr.Expressions.Cache;
using Embarr.Expressions.Services;

namespace Embarr.Expressions
{
    public static class MethodExpressions
    {
        /// <summary>
        /// Gets the MethodInfo from a lambda expression.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo(this LambdaExpression lambdaExpression)
        {
            Func<Expression, MethodInfo> getMethodInfo = MethodInfoService.GetMethodInfoFromExpression;
            return getMethodInfo.Memoize()(lambdaExpression.Body);
        }

        /// <summary>
        /// Gets the MethodInfo from a lambda expression.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="methodExpression">The method expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo<TModel>(Expression<Action<TModel>> methodExpression)
        {
            Func<Expression, MethodInfo> getMethodInfo = MethodInfoService.GetMethodInfoFromExpression;
            return getMethodInfo.Memoize()(methodExpression.Body);
        }

        /// <summary>
        /// Determines whether the specified lambda expression is a method.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static bool IsMethod(this LambdaExpression lambdaExpression)
        {
            Func<Expression, bool> getMethodInfo = MethodInfoService.IsMethod;
            return getMethodInfo.Memoize()(lambdaExpression.Body);
        }

        /// <summary>
        /// Determines whether the specified method expression is a method.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="methodExpression">The method expression.</param>
        /// <returns></returns>
        public static bool IsMethod<TModel>(Expression<Action<TModel>> methodExpression)
        {
            Func<Expression, bool> getMethodInfo = MethodInfoService.IsMethod;
            return getMethodInfo.Memoize()(methodExpression.Body);
        }

        /// <summary>
        /// Converts expression to a MethodCallExpression.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static MethodCallExpression ToMethodCallExpression(this LambdaExpression lambdaExpression)
        {
            return lambdaExpression.Body.ToMethodCallExpression();
        }

        /// <summary>
        /// Converts expression to a MethodCallExpression.
        /// </summary>
        /// <param name="methodExpression">The lambda expression.</param>
        /// <returns></returns>
        public static MethodCallExpression ToMethodCallExpression<TModel>(Expression<Action<TModel>> methodExpression)
        {
            return MethodCallExpressionService.ToMethodCallExpression(methodExpression);
        }

        /// <summary>
        /// Calls the method.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="model">The model.</param>
        public static void CallMethod<TModel>(this LambdaExpression lambdaExpression, TModel model)
        {
            lambdaExpression.Compile().DynamicInvoke(model);
        }

        /// <summary>
        /// Calls the method.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="methodExpression">The method expression.</param>
        /// <param name="model">The model.</param>
        public static void CallMethod<TModel>(Expression<Action<TModel>> methodExpression, TModel model)
        {
            methodExpression.Compile()(model);
        }

        /// <summary>
        /// Calls the method.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TReturn">The type of the return.</typeparam>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static TReturn CallMethod<TModel, TReturn>(this LambdaExpression lambdaExpression, TModel model)
        {
            return (TReturn)lambdaExpression.Compile().DynamicInvoke(model);
        }

        /// <summary>
        /// Calls the method.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TReturn">The type of the return.</typeparam>
        /// <param name="methodExpression">The method expression.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static TReturn CallMethod<TModel, TReturn>(Expression<Func<TModel, TReturn>> methodExpression, TModel model)
        {
            return methodExpression.Compile()(model);
        }
    }
}