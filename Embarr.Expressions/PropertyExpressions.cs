﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Embarr.Expressions.Cache;
using Embarr.Expressions.Services;

namespace Embarr.Expressions
{
    public static class PropertyExpressions
    {
        /// <summary>
        /// Gets the PropertyInfo from a lambda expression.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static PropertyInfo GetPropertyInfo(this LambdaExpression lambdaExpression)
        {
            Func<LambdaExpression, PropertyInfo> getPropertyInfo = PropertyInfoService.GetPropertyInfoFromExpression;
            return getPropertyInfo.Memoize()(lambdaExpression);
        }

        /// <summary>
        /// Gets the PropertyInfo from a lambda expression.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="propertyExpression">The property expression.</param>
        /// <returns></returns>
        public static PropertyInfo GetPropertyInfo<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> propertyExpression)
        {
            Func<LambdaExpression, PropertyInfo> getPropertyInfo = PropertyInfoService.GetPropertyInfoFromExpression;
            return getPropertyInfo.Memoize()(propertyExpression);
        }

        /// <summary>
        /// Determines whether the specified lambda expression is a property.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static bool IsProperty(this LambdaExpression lambdaExpression)
        {
            Func<LambdaExpression, bool> isProperty = PropertyInfoService.IsProperty;
            return isProperty.Memoize()(lambdaExpression);
        }

        /// <summary>
        /// Determines whether the specified property expression is a property.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="propertyExpression">The property expression.</param>
        /// <returns></returns>
        public static bool IsProperty<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> propertyExpression)
        {
            Func<LambdaExpression, bool> isProperty = PropertyInfoService.IsProperty;
            return isProperty.Memoize()(propertyExpression);
        }

        /// <summary>
        /// Determines whether the lambda expression is pointing to a property indexer.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static bool IsPropertyIndexer(this LambdaExpression lambdaExpression)
        {
            Func<LambdaExpression, bool> isPropertyIndexer = PropertyInfoService.IsPropertyIndexer;
            return isPropertyIndexer.Memoize()(lambdaExpression);
        }

        /// <summary>
        /// Determines whether the lambda expression is pointing to a property indexer.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="propertyExpression">The property expression.</param>
        /// <returns></returns>
        public static bool IsPropertyIndexer<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> propertyExpression)
        {
            Func<LambdaExpression, bool> isPropertyIndexer = PropertyInfoService.IsPropertyIndexer;
            return isPropertyIndexer.Memoize()(propertyExpression);
        }

        /// <summary>
        /// Gets the value from an object using the lambda expression.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static TProperty GetValue<TModel, TProperty>(this LambdaExpression lambdaExpression, TModel model)
        {
            return (TProperty)lambdaExpression.Compile().DynamicInvoke(model);
        }

        /// <summary>
        /// Gets the value from an object using the property expression.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="propertyExpression">The property expression.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static TProperty GetValue<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> propertyExpression, TModel model)
        {
            return propertyExpression.Compile()(model);
        }

        /// <summary>
        /// Converts expression into a member expression.
        /// </summary>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <returns></returns>
        public static MemberExpression ToMemberExpression(this LambdaExpression lambdaExpression)
        {
            if (!lambdaExpression.IsProperty())
            {
                throw new ArgumentException("Only properties are supported");
            }

            Func<LambdaExpression, MemberExpression> getMemberExpression = PropertyInfoService.GetMemberExpression;
            return getMemberExpression.Memoize()(lambdaExpression);
        }

        /// <summary>
        /// Converts expression into a member expression.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="propertyExpression">The property expression.</param>
        /// <returns></returns>
        public static MemberExpression ToMemberExpression<TModel, TProperty>(Expression<Func<TModel, TProperty>> propertyExpression)
        {
            if (!propertyExpression.IsProperty())
            {
                throw new ArgumentException("Only properties are supported");
            }

            Func<LambdaExpression, MemberExpression> getMemberExpression = PropertyInfoService.GetMemberExpression;
            return getMemberExpression.Memoize()(propertyExpression);
        }
    }
}
