﻿using System;
using System.Linq.Expressions;

namespace Embarr.Expressions.Services
{
    internal static class MethodCallExpressionService
    {
        internal static MethodCallExpression ToMethodCallExpression(this Expression lambdaExpressionBody)
        {
            if (lambdaExpressionBody as MethodCallExpression != null)
            {
                return (MethodCallExpression)lambdaExpressionBody;
            }

            if (lambdaExpressionBody as LambdaExpression != null)
            {
                var lambdaExpression = ((LambdaExpression) lambdaExpressionBody);
                if (lambdaExpression.Body as MethodCallExpression != null)
                {
                    return (MethodCallExpression)lambdaExpression.Body;
                }
            }

            throw new NotSupportedException("Expression is not a method expression.");
        } 
    }
}