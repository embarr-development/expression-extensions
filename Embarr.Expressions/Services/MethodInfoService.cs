﻿using System.Linq.Expressions;
using System.Reflection;

namespace Embarr.Expressions.Services
{
    internal static class MethodInfoService
    {
        internal static MethodInfo GetMethodInfoFromExpression(this Expression lambdaExpressionBody)
        {
            return lambdaExpressionBody.ToMethodCallExpression().Method;
        }

        internal static bool IsMethod(this Expression lambdaExpressionBody)
        {
            return lambdaExpressionBody as MethodCallExpression != null;
        }
    }
}