﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Embarr.Expressions.Services
{
    internal static class PropertyInfoService
    {
        internal static PropertyInfo GetPropertyInfoFromExpression(LambdaExpression lambdaExpression)
        {
            var lambdaExpressionBody = lambdaExpression.Body;

            if (lambdaExpressionBody as MemberExpression != null)
            {
                return ((MemberExpression)lambdaExpressionBody).Member as PropertyInfo;
            }

            if (lambdaExpressionBody as UnaryExpression != null)
            {
                var unary = ((UnaryExpression)lambdaExpressionBody);
                return ((MemberExpression)unary.Operand).Member as PropertyInfo;
            }

            throw new NotSupportedException("Not supported at the moment");
        }

        internal static bool IsProperty(LambdaExpression lambdaExpression)
        {
            var lambdaExpressionBody = lambdaExpression.Body;

            if (lambdaExpressionBody as MemberExpression != null)
            {
                return ((MemberExpression)lambdaExpressionBody).Member is PropertyInfo;
            }

            if (lambdaExpressionBody as UnaryExpression != null)
            {
                var unary = ((UnaryExpression)lambdaExpressionBody);
                return ((MemberExpression)unary.Operand).Member is PropertyInfo;
            }

            return false;
        }

        internal static bool IsPropertyIndexer(this LambdaExpression expression)
        {
            var methodCallExpression = expression.Body as MethodCallExpression;
            return methodCallExpression != null && methodCallExpression.Method.IsSpecialName;
        }

        internal static MemberExpression GetMemberExpression(LambdaExpression lambdaExpression)
        {
            var lambdaExpressionBody = lambdaExpression.Body;

            if (lambdaExpressionBody as MemberExpression != null)
            {
                return (MemberExpression)lambdaExpressionBody;
            }

            if (lambdaExpressionBody as UnaryExpression != null)
            {
                var unary = ((UnaryExpression)lambdaExpressionBody);
                return (MemberExpression)unary.Operand;
            }

            throw new NotSupportedException("Could not create member expression");
        }
    }
}