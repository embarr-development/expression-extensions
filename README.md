# Expression Extensions #

A collection of useful extensions for expressions in .Net.

*There is an in-built memoize cache for improving the performance of any repeat calls.*

## Property Extensions ##

*Expression<Func<TestModel, string>> myPropertyExpression = x => x.MyProperty;*

### GetPropertyInfo ###

```
#!c#

PropertyInfo myProperty = myPropertyExpression.GetPropertyInfo();
```

or

```
#!c#

PropertyInfo myProperty = PropertyExpressions.GetPropertyInfo<TestModel, object>(x => x.MyProperty);
```

### IsProperty ###

```
#!c#

bool isProperty = myPropertyExpression.IsProperty()
```

or

```
#!c#

bool isProperty = PropertyExpressions.IsProperty<TestModel, string>(x => x.SubTestModelList[0].MyProperty);
```

### IsPropertyIndexer ###

```
#!c#

bool isPropertyIndexer = myPropertyExpression.IsPropertyIndexer()
```

or

```
#!c#

bool isPropertyIndexer = PropertyExpressions.IsPropertyIndexer<TestModel, object>(x => x.MyList[0]);
```

### GetValue ###

```
#!c#

string myValue = myPropertyExpression.GetValue<TestModel, string>(testModel);
```

or

```
#!c#

string myValue = PropertyExpressions.GetValue(myPropertyExpression, testModel);
```

### ToMemberExpression ###

```
#!c#

var memberExpression = myPropertyExpression.ToMemberExpression();
```

or

```
#!c#

var memberExpression = PropertyExpressions.ToMemberExpression<TestModel, string>(x => x.MyProperty);
```

## Method Extensions ##

*Expression<Action<TestModel>> myMethodExpression = x => x.MyMethod("123");*

*Expression<Func<TestModel, string>> myMethodWithReturnExpression = x => x.MyMethodWithReturn("123");*

### GetMethodInfo ###

```
#!c#

MethodInfo myMethod = myMethodExpression.GetMethodInfo();
```

or

```
#!c#

MethodInfo myMethod = MethodExpressions.GetMethodInfo<TestModel>(x => x.MyMethod("123"));
```
### IsMethod ###

```
#!c#

bool isMethod = myMethodExpression.IsMethod();
```

or

```
#!c#

bool isMethod = MethodExpressions.IsMethod<TestModel>(x => x.MyMethod("123"));
```
### ToMethodCallExpression ###

```
#!c#

MethodCallExpression expression = myMethodExpression.ToMethodCallExpression();
```

or

```
#!c#

MethodCallExpression expression = MethodExpressions.ToMethodCallExpression<TestModel>(x => x.MyMethod("123"));
```
### CallMethod ###

```
#!c#

myMethodExpression.CallMethod(testModel);
string result = myMethodWithReturnExpression.CallMethod<TestModel, string>(testModel);
```

or

```
#!c#

MethodExpressions.CallMethod(myMethodExpression, testModel);
var result = MethodExpressions.CallMethod(myMethodWithReturnExpression, testModel);
```